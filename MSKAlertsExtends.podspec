Pod::Spec.new do |s|
  s.name         = "MSKAlertsExtends"
  s.version      = "0.0.1"
  s.summary      = "MSK exnted classes"
  s.description  = "MSK exnted classes..."
  s.homepage     = "http://www.littlegleam.com"
  s.license      = { :type => 'MIT', :file => 'LICENSE' }
  s.author       = { 'Masaki Oshikawa' => 'oshikawakanu+bitbucket@gmail.com' }
  s.source       = { :git => "https://moshikawa@bitbucket.org/moshikawa/mskalertsextends.git", :tag => s.version.to_s }
  s.platform     = :ios, '7.0'
  s.source_files = 'MSKAlertsExtends/*.{h,m}'
  s.dependency 'SIAlertView', '~> 1.0'
  s.dependency 'SVProgressHUD'
#s.dependency	 = 'SVProgressHUD'
#  s.dependency 'SIAlertView', :git => 'https://github.com/Sumi-Interactive/SIAlertView.git'
#  s.dependency 'SVProgressHUD', :git => 'https://github.com/TransitApp/SVProgressHUD.git'
#  s.dependency 'SIAlertView', :git => 'https://github.com/Sumi-Interactive/SIAlertView.git'
#  s.dependency 'SIAlertView', :commit => '747e53da84648e96b3e726665b2a6455f1ab995b'
#  s.dependency 'SVProgressHUD', :git => 'https://github.com/TransitApp/SVProgressHUD.git', :commit => '9e2a22dee810d7da2b93e795b2d92b1e08a41ea1'

  s.requires_arc = true
end

