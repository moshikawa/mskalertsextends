//
//  SIAlertView+MSKExtends.m
//  nchanco
//
//  Created by admin on 2013/10/17.
//  Copyright (c) 2013年 Little Gleam. All rights reserved.
//

#import "SIAlertView+MSKExtends.h"

@implementation SIAlertView (MSKExtends)

+ (SIAlertView *)msk_showError:(NSError *)error {
	return [self msk_showAlertWithTitle:@"Error" message:error.localizedDescription];
}

+ (SIAlertView *)msk_defaultStyleAlert {
	SIAlertView *result = [[self alloc] initWithTitle:nil andMessage:nil];
	result.transitionStyle = SIAlertViewTransitionStyleDropDown;
	return result;
}

// アラートの表示
+ (SIAlertView *)msk_alertWithTitle:(NSString *)title message:(NSString *)message {
	SIAlertView *result = [[self alloc] initWithTitle:title andMessage:message];
	result.transitionStyle = SIAlertViewTransitionStyleDropDown;
	[result addButtonWithTitle:@"OK" type:SIAlertViewButtonTypeCancel handler:nil];
	return result;
}

+ (SIAlertView *)msk_showAlertWithTitle:(NSString *)title message:(NSString *)message {
	SIAlertView *result = [self msk_alertWithTitle:title message:message];
	[result show];
	return result;
}

// はい、いいえ
+ (SIAlertView *)msk_confirmWithTitle:(NSString *)question message:(NSString *)message handler:(SIAlertViewHandler)handler {
	SIAlertView *result = [[self alloc] initWithTitle:question andMessage:message];
	result.transitionStyle = SIAlertViewTransitionStyleDropDown;
	[result addButtonWithTitle:@"Cancel" type:SIAlertViewButtonTypeCancel handler:nil];
	[result addButtonWithTitle:@"OK" type:SIAlertViewButtonTypeDefault handler:handler];
	[result show];
	return result;
}


@end
