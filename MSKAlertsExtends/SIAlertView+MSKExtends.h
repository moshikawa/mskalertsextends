//
//  SIAlertView+MSKExtends.h
//  nchanco
//
//  Created by admin on 2013/10/17.
//  Copyright (c) 2013年 Little Gleam. All rights reserved.
//

#import <SIAlertView/SIAlertView.h>

@interface SIAlertView (MSKExtends)

+ (SIAlertView *)msk_showError:(NSError *)error;
+ (SIAlertView *)msk_defaultStyleAlert;

// アラートの表示
+ (SIAlertView *)msk_alertWithTitle:(NSString *)title message:(NSString *)message;
+ (SIAlertView *)msk_showAlertWithTitle:(NSString *)title message:(NSString *)message;
+ (SIAlertView *)msk_confirmWithTitle:(NSString *)question message:(NSString *)message handler:(SIAlertViewHandler)handler;

@end
